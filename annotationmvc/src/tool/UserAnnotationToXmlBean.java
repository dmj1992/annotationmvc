package tool;

import java.util.HashMap;
import java.util.Map;

import annotation.AnnotationXml;
import model.XmlBean;

public class UserAnnotationToXmlBean {
	static XmlBean xmlBean = new XmlBean();

	public static Map<String, XmlBean> transfer(AnnotationXml myAnnotation) {
		xmlBean.setActionClass(myAnnotation.actionClass());
		xmlBean.setActionType(myAnnotation.actionType());
		xmlBean.setBeanName(myAnnotation.beanName());
		xmlBean.setFormClass(myAnnotation.formClass());
		xmlBean.setPath(myAnnotation.path());
		Map<String, XmlBean> rmap = new HashMap<String, XmlBean>();
		Map<String, String> map = new HashMap<String, String>();
		String[] forwardname=myAnnotation.forwardname();
		String[] forwardvalue=myAnnotation.forwardvalue();
		for (int i = 0; i < forwardname.length; i++) {
            map.put(forwardname[i], forwardvalue[i]);
		}
		xmlBean.setActionForward(map);
		rmap.put(xmlBean.getPath(), xmlBean);
		return rmap;

	}
}
