package tool;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import model.ActionForm;
//填充实体类
public class FullForm {

	public FullForm() {

	}

	public static ActionForm full(String forpath, HttpServletRequest req) {
		ActionForm actionForm = null;
		try {
			Class clazz = Class.forName(forpath);
			actionForm = (ActionForm) clazz.newInstance();// newInatance是必须有默认构造器
			Field[] farr = clazz.getDeclaredFields();
			for (Field field : farr) {
				// System.out.println("字段："+field.getName());
				field.setAccessible(true);
				field.set(actionForm, req.getParameter(field.getName()));
				field.setAccessible(false);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return actionForm;
	}

}
