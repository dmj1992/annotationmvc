package tool;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import model.XmlBean;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class Struts_config {

	/**
	 * @param args
	 * @throws IOException
	 * @throws JDOMException
	 */
	public static Map<String, XmlBean> struts_xml(String path1)
			throws JDOMException, IOException {
		// TODO Auto-generated method stub
		SAXBuilder sax = new SAXBuilder();
		// "config/struts-config.xml"
		Document document = sax.build(path1);
		Element root = document.getRootElement();
		// ===================================================
		// Element actionform = root.getChild("formbeans");
		// List<Element> form = actionform.getChildren();
		// System.out.println(form);
		// for (Element element : form) {
		// String name = element.getAttributeValue("name");
		// String clas = element.getAttributeValue("class");
		// System.out.println(name + "====" + clas);
		// }
		// ====================================================
		Element actionroot = root.getChild("action-mapping");
		List<Element> actio = actionroot.getChildren();
		// System.out.println(actio);
		Map<String, XmlBean> rmap = new HashMap<String, XmlBean>();
		for (Element element : actio) {
			XmlBean action = new XmlBean();
			String name = element.getAttributeValue("name");
			action.setBeanName(name);
			Element actionform = root.getChild("formbeans");
			List<Element> form = actionform.getChildren();
			// System.out.println(form);
			for (Element ex : form) {
				// String name = ex.getAttributeValue("name");
				// String clas = ex.getAttributeValue("class");
				// System.out.println(name + "====" + clas);
				if (name.equals(ex.getAttributeValue("name"))) {
					String formClass = ex.getAttributeValue("class");
					action.setFormClass(formClass);
					break;
				}
			}
			String path = element.getAttributeValue("path");
			action.setPath(path);
			String type = element.getAttributeValue("type");
			action.setActionType(type);
			// System.out.println(name + "====" + path + "====" + type);
			List<Element> forward = element.getChildren();
			Map<String, String> map = new HashMap<String, String>();
			for (Element forw : forward) {
				String fname = forw.getAttributeValue("name");
				String fvalue = forw.getAttributeValue("value");
				// System.out.println(name + "====" + fvalue);
				map.put(fname, fvalue);
			}
			action.setActionForward(map);
			rmap.put(path, action);
		}
		return rmap;
	}

	public static void main(String[] args) throws JDOMException, IOException {
		Map<String, XmlBean> map = struts_xml("config/struts-config.xml");
		Set<String> set = map.keySet();
		for (Iterator iterator = set.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			XmlBean xmlBean = map.get(string);
			System.out.println(string + "===" + xmlBean);
		}
	}
}
