package model;

import java.util.HashMap;
import java.util.Map;

public class XmlBean {

	public XmlBean() {
	}

	private String beanName;
	private String path;
	private String actionClass;
	private String formClass;
	private String actionType;
	private Map<String, String> actionForward = new HashMap<String, String>();

	public String getBeanName() {
		return beanName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getActionClass() {
		return actionClass;
	}

	public void setActionClass(String actionClass) {
		this.actionClass = actionClass;
	}

	public String getFormClass() {
		return formClass;
	}

	public void setFormClass(String formClass) {
		this.formClass = formClass;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public Map<String, String> getActionForward() {
		return actionForward;
	}

	public void setActionForward(Map<String, String> actionForward) {
		this.actionForward = actionForward;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.beanName + "---" + this.formClass + "---" + this.actionType
				+ "---" + this.actionForward;
	}
}
