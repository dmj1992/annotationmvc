package listener;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import model.XmlBean;

import org.jdom.JDOMException;

import annotation.AnnotationXml;
import annotation.UserAnnotation;

import tool.Struts_config;
import tool.UserAnnotationToXmlBean;

public class ActionListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("信息已经注销");
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		ServletContext context = arg0.getServletContext();
		Map<String, XmlBean> map = null;
		UserAnnotation userannotation = new UserAnnotation();
		Class<UserAnnotation> clazz = UserAnnotation.class;
		try {
			Method method = clazz.getMethod("login",new Class[]{});
			if(method.isAnnotationPresent(AnnotationXml.class)){
				//如果存在该元素的指定类型的注释，则返回这些注释，否则返回 null。
				AnnotationXml myAnnotation= method.getAnnotation(AnnotationXml.class);
			    map=UserAnnotationToXmlBean.transfer(myAnnotation);
			}
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		context.setAttribute("struts", map);
	}

}
