package controller;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.ActionForm;


public interface Action {
	String excute(HttpServletRequest req, ActionForm actionForm,
			Map<String, String> actionForward);
}
