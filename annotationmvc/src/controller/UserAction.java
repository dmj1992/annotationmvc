package controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import model.ActionForm;
import model.UserForm;

public class UserAction implements Action {

	@Override
	public String excute(HttpServletRequest req, ActionForm actionForm,
			Map<String, String> actionForward) {
		// TODO Auto-generated method stub
		// 定义返回的值
		String url = "fail";
		// 将父类转化为对应的子类
		UserForm sForm = (UserForm) actionForm;
		System.out.println(sForm.getName()+"**********"+sForm.getPass());
		if (sForm.getName().equals("aaa") & sForm.getPass().equals("bbb")) {
			url = "succ";
		}
		return actionForward.get(url);
	}

	// @Override
	// public String excute(HttpServletRequest req, ActionForm actionForm,
	// Map<String, String> actionForward) {
	// // TODO Auto-generated method stub
	// String url="shibai";
	// SForm sForm=(SForm) actionForm;
	// if(sForm.getName().equals("dmj")){
	// url="chenggong";
	// }
	// return actionForward.get(url);
	// }

}
