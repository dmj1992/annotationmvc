package controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.ActionForm;
import model.XmlBean;
import tool.FullForm;

public class ActionServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取的请求路径
		String path = this.getServerPath(request.getServletPath());
		// 获取到context里面name为struts的对应映射的path
		Map<String, XmlBean> map = (Map<String, XmlBean>) this
				.getServletContext().getAttribute("struts");
		// 通过path获取到解析到的xml的节点信息
		XmlBean xmlBean = map.get(path);
		Map<String, String> map111=xmlBean.getActionForward();
		//System.out.println("-------------"+map111.get("succ"));
		// 获取的实体对象的路径
		String formClass = xmlBean.getFormClass();
		// 将页面的数据绑定到对应的实体对象中
		ActionForm actionForm = FullForm.full(formClass, request);
		// 获取到action对应的class路径
		String actiontype = xmlBean.getActionType();
		Action action = null;
		String url = null;
		try {
			Class clazz = Class.forName(actiontype);
			action = (Action) clazz.newInstance();
			url = action
					.excute(request, actionForm, xmlBean.getActionForward());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 获得转发器对象
		System.out.println("确定转发的路径为："+url);
		RequestDispatcher dis = request.getRequestDispatcher(url);
		dis.forward(request, response);
	}

	// 获取请求url的后面路径，截调后缀
	private String getServerPath(String ServerPath) {
		ServerPath = ServerPath.split("\\.")[0];
		return ServerPath;
	}
}
