package annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface AnnotationXml {
	String beanName();
	String path();
	String actionClass();
	String formClass();
	String actionType();
	String[] forwardname();
	String[] forwardvalue();
}
/*enum forward{
	
}*/