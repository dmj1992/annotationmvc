package annotation;


public class UserAnnotation {
	@AnnotationXml( actionClass = "user", 
            actionType = "controller.UserAction", 
            beanName = "user", 
            formClass = "model.UserForm", 
            forwardname = {"succ","fail"},
            forwardvalue={"/view/usersucc.jsp","/view/userfail.jsp"},
            path = "/userLogin")
    public void login(){
    	
    }
}
